import asyncio
import ssl
import logging

logging.basicConfig(level=logging.DEBUG)

class IRCServer:
    def __init__(self, username: str, nick: str, password: str):
        self.username = username
        self.nick = nick
        self.password = password
        self.logger = logging.getLogger('IRCServer')
        self.disconnected = asyncio.Event()

    async def connect(self, host: str, port: int=6697, password: str=None, use_ssl: bool=True, loop: asyncio.AbstractEventLoop=None):
        if not loop:
            loop = asyncio.get_event_loop()

        if use_ssl:
            sc = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile='selfsigned.cert')
            sc.check_hostname = False
            sc.verify_mode = ssl.CERT_NONE

        self.logger.info('Opening Connection...')
        self.reader, self.writer = await asyncio.open_connection(host,
                                                                 port,
                                                                 ssl=sc if use_ssl else use_ssl,
                                                                 loop=loop)
        self.logger.info('Connection Established.')
        loop.create_task(self.message_handler())

    async def writeln(self, msg):
        if isinstance(msg, str):
            msg = msg.encode('utf-8')
        self.writer.write(msg + b'\r\n')
        await self.writer.drain()
        self.logger.debug(f'Sent: {msg.decode("utf-8")}')

    async def register(self, mode: str='+iB'):
        if self.password:
            await self.writeln(f'PASS {self.password}\n')
        await self.writeln(f'USER {self.username} {mode if mode else "*"} {self.username} :{self.username}\n')
        await self.writeln(f'NICK {self.nick}\n')
        self.logger.info('Registration info sent')

    async def message_handler(self):
        self.logger.info('Handler started...')
        while not self.disconnected.is_set():
            response = (await self.reader.readline()).decode('utf-8')
            self.logger.debug(response)
            if response.startswith('PING'):
                msg = response.split(':', 1)[1].strip()
                await self.ping(msg)
            elif response.startswith('ERROR :Closing Link:'):
                self.disconnected.set()
                msg = response.split(':', 2)[-1].strip()
                self.logger.info(f'Link Closed By Remote Host {msg}')

    async def ping(self, message: str=None):
        if not message:
            message = self.username
        await self.writeln(f'PONG :{message}')

loop = asyncio.get_event_loop()

async def run_bot():
    serv = IRCServer('Geeksbot', 'Geeksbot', None)
    await serv.connect('irc.sebisbottutorial.com')
    await serv.register()
    while True:
        await asyncio.sleep(1)

loop.run_until_complete(run_bot())
