import logging
import asyncio
import server
import ssl
import message


class Client:
    def __init__(self, *, nick: str, username: str, password: str=None, prefix: str='!'):
        self.nick = nick
        self.username = username
        self.password = password
        self.prefix = prefix
        self.servers = {}
        self.loop = None
        self.logger = logging.getLogger('Client')

    async def run(self, servers: dict, loop: asyncio.AbstractEventLoop=None):
        self.loop = loop or asyncio.get_event_loop()

        for name, s in servers.items():
            serv = await self.connect_to_server(host=s['host'], port=s['port'], server_pass=s['pass'], use_ssl=s['ssl'])
            self.loop.create_task(serv.queue_handler())
            await self.register(serv)
            self.loop.create_task(self.message_handler(serv))
            channels = ','.join([name for name, _ in s['channels']])
            channel_passwords = ','.join([password for _, password in s['channels']])
            await serv.join(channels, channel_passwords)
            self.servers[name] = serv

    async def connect_to_server(self, *, host: str, port: int=6697, server_pass: str=None, use_ssl: bool=True):
        if use_ssl:
            sc = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile='selfsigned.cert')
            sc.check_hostname = False
            sc.verify_mode = ssl.CERT_NONE

        serv = server.IRCServer(host=host, port=port, password=server_pass, ssl=sc if use_ssl else use_ssl)
        await serv.connect()
        return serv

    async def register(self, serv: server.IRCServer, mode: str='+iB'):
        if self.password:
            await serv.add_queue(f'PASS {self.password}\n')
        await serv.add_queue(f'USER {self.username} {mode if mode else "*"} {self.username} :{self.username}\n')
        await serv.add_queue(f'NICK {self.nick}\n')
        srv_command = None
        while srv_command != '433' and srv_command != '001':
            response = await serv.readline()
            origin, srv_command, content = await self.process_message(response, serv)

        if srv_command == '433':
            raise RuntimeError('Nickname is already taken. Are you trying to run two bots at once?')
        elif srv_command == '001':
            self.logger.info('Client Registration Sucessfull')
            return 1

    async def process_message(self, message: str, serv: server.IRCServer):
        if message.startswith('PING'):
            msg = message.split(':', 1)[1].strip()
            await serv.pong(msg)
            return None, 'PING', ':' + msg
        elif message.startswith('ERROR :Closing Link:'):
            serv.disconnected.set()
            msg = message.split(':', 2)[-1].strip()
            self.logger.info(f'Link Closed By Remote Host {msg}')
            return None, 'ERROR', ':' + msg
        else:
            if message.startswith(':'):
                origin, command, content = message.split(' ', 2)
                origin = origin[1:]
            else:
                origin = None
                command, content = message.split(' ', 1)
            return origin, command, content

    async def message_handler(self, serv: server.IRCServer):
        self.logger.info('Handler started...')
        while not serv.disconnected.is_set():
            response = await serv.readline()
            self.logger.debug(response)
            origin, command, content = await self.process_message(response, serv)
            self.logger.debug(f'origin: {origin} | command: {command} | content: {content}')
            if command == 'PRIVMSG':
                msg = message.PRIVMSG(irc_command=command, origin=origin, contents=content, server=serv)
                if msg.is_command():
                    await msg.reply(f'command:{msg.command} | args:{msg.args}')
            else:
                msg = message.Message(irc_command=command, origin=origin, contents=content, server=serv)
 
